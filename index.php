<html>
    <head>
        <meta charset="utf-8" />
        <title>KRADAC Cia. Ltda.</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="shortcut icon" href="icon_kradac.png" type="image/x-icon" />
    </head>
    <body>
    <center>
        <br/>
        <br/>
        <br/>
        <img src="logo.png" alt="KRADAC Cia. Ltda."/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div style="display:table; table-layout:fixed; ">
            <div style="display:table-row;  ">
                <div style="display:table-cell; ">
                    <a href="http://200.0.29.117:8080/KPOINT" class="button" style="top: 217px; left: 143px; ">K-Point</a>
                    <p style="text-align:center">Puntos de Interes</p>
                </div>
                <div style="width:10px">
                </div>
                <div style="display:table-cell; ">
                    <a href="http://200.0.29.117:8080/Tour360" class="button" style="top: 217px; left: 143px; ">K-Tour</a>
                    <p style="text-align:center">Recorridos en 3D</p>
                </div>
                <div style="width:10px">
                </div>
                <div style="display:table-cell; ">
                    <a href="http://200.0.29.117:8080/ksensor" class="button"
                       style="top: 217px; left: 143px; ">K-Sensor</a>
                    <br/>
                    <p style="text-align:center">Medidor de Temperatura</p>
                </div>
            </div>

            <div style="width:10px; height:50px" >
            </div>
            <div style="display:table-row; ">
                <div style="display:table-cell; "></div>
                <div style="width:10px">
                </div>
                <div style="display:table-cell; "></div>
                <div style="width:10px">
                </div>
                <div style="display:table-cell; "></div>
            </div>
        </div>
        <div style="display:table; table-layout:fixed; ">
            <div style="display:table-row; ">
            </div>
            <div style="display:table-cell; ">
                <a href="../karview" class="button"
                   style="top: 217px; left: 143px; ">KarView</a>
                <br/><p style="text-align:center">Rastreo CoopMego</p>
            </div>
            <div style="width:10px">
            </div>
            <div style="display:table-cell; ">
                <a href="http://200.0.29.117:8080/k-logger" class="button"
                   style="top: 217px; left: 143px; ">K-Logger</a>
                <br/><p style="text-align:center">Telemetría y Recepción<br> de variables</p>
            </div>
            <div style="width:10px">
            </div>
            <div style="display:table-cell; ">
                <a href="../k-taxy" class="button"
                   style="top: 217px; left: 143px; ">K-Taxy</a>
                <br/><p style="text-align:center">Rastreo de taxis</p>
            </div>
            <div style="width:10px">
            </div>
            <div style="display:table-cell; ">
                <a href="../Inicio/kbus.html" class="button"
                   style="top: 217px; left: 143px; ">K-Bus</a>
                <br/><p style="text-align:center">Rastreo de buses</p>
            </div>
            <div style="width:10px">
            </div>
            <div style="display:table-cell; ">
                <a href="http://200.0.29.117:8080/k-parking" class="button"
                   style="top: 217px; left: 143px; ">K-Parking</a>
                <br/><p style="text-align:center">Parqueaderos</p>
            </div>
        </div>
</center>
</body>

</html>
